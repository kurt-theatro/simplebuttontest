/*! \mainpage Interface Library
*
* \section intro_sec Introduction
*
* The interface library provides hardware abstraction layer for simpler
* software development. The core interface library is designed to register
* callback events so that application layer modules can register event
* function execution instead of polling. This will help optimize CPU usage
* and eventually simple code designs.
*
* \section Document Version
* -# 18-Feb-2019 Draft 0.1 - Created initial draft version
* -# 14-Mar-2019 Draft 0.2
*	- Modified@n
*		- Section 4.1 – Removed audio data and added interface library details.@n
*		- Section 4.2 – Added button callback functionality@n
* -# 17-Mar-2019 Draft 0.3
*	- New document generated from header file itself
*	- Added API for all peripherals
* -# 20-Mar-2019 Draft 0.4
*	- Added@n
*		- Section 5.1.4.29 - API to amplify playback volume
*		- Section 5.1.4.27 - API to amplify capture volume
*		- Section 5.1.3.8  - Added enum for RTC alarm interrupt
*		- Section 5.3.1.2  - Added internal ADC in E_ADC_CHANNELS
*		- Section 5.1.4.13 - Added API to read I2C register
*		- Section 5.3.1.33 - Added API to write I2C register
*	- Modified@n
*		- Section 5.1.3.8  - All events merged into single ENUM
*		- Section 5.1.4.36 - SetLedColor API includes LED brightness as input parameter
*
**/

/**
* @file LibInterface.h
* @author eInfochips
* @date 21 Mar 2019
* @brief This file contain Application Program Interface (API) for
*        Communicator3 peripherals
*
* Header file of Interface library. This contains list of API for all
* peripherals
*
*/
#ifndef __LIBINTERFACE_H__
#define __LIBINTERFACE_H__

#include <sys/inotify.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "Rules.h"

/*!
* @brief This eNUM lists all the button available on C3 hardware along with
*        audio jack detect.
*
* Detailed explanation.
*
* All the button interrupts are registered as input events. All four button are
* registered on event0 dev node and audio jack detection interrupt is channeled
* on separate (event1) dev node. User can used already defined macros to
* register callback on either event0 or event1
*
*/

extern char *EVENT2STR [];

typedef enum {

/** Used this enum to while registering callback for center command button */
	EVENT_BUTTON_COMMAND_BUTTON,

/** Used this enum to while registering callback for Volume plus button */
	EVENT_BUTTON_VOLUME_PLUS,

/** Used this enum to while registering callback for Volume minus button */
	EVENT_BUTTON_VOLUME_MINUS,

/** Used this enum to while registering callback for Push To Talk
* (PTT) button */
	EVENT_BUTTON_PTT,

/** Detect audio jack connection and disconnection event */
	EVENT_AUDIO_JACK_DETECT,

/** The charger identifies input source based on the type of USB device
* connected. In case charging is done through pogo pins then "Unknown USB"
* device status is set. In this case charging current is limited to 500mA
*/
	EVENT_CHARGER_SRC_CONNECT,

/** This enum will provide status of charging. Here charging should be enabled.
* This API is not designed to provide status, but generate and event in case
* the status is changed. User application should call GetChargerStatus API to
* get actual value from IC. Below are the possible charging status.@n
*	-# Not charging,@n
*	-# pre charge,@n
*	-# Fast charge,@n
*	-# Charge termination
*/
	EVENT_CHARGE_STATUS,

/** This enum provide status of power on input of battery charger. In case
* voltage is above UVLO and below OVP then it is considered as Power good
* signal.
*/
	EVENT_CHARGE_POWERGOOD,

/** Provide status from charging@n
*	-# Normal,@n
*	-# OVP or VBAT < 3.8V,@n
*	-# Thermal shutdown,@n
*	-# Charger safety time expire@n
*/
	EVENT_CHARGE_FAULT,

/** Provides status of battery with respect to charging@n
*	-# Normal,@n
*	-# BATOVP@n
*/
	EVENT_BATTERY_FAULT,

/** This enum provides status of charge failures on account of un even
* temperature@n
*	-# Normal,@n
*	-# Warm,@n
*	-# Cool,@n
*	-# Cold,@n
*	-# Hot@n
*/
	EVENT_NTC_FAULT,

/** The battery gauge recieves an interrupt based on the low SOC threshold set
* in the battery gauge IC. Currently the threshold is set to 10%. Which means
* Low SoC event will be triggered when battery capacity goes 10% or below.
*/
	EVENT_GAUGE_LOW_SOC,

/** Use this event to register callback for temperature event from Gauge. Li-on
* battery are supposed to be operated in range of 0-40 degrees only. In case the
* temperature varies from above range then it is recommended to stop the charging
* of the battery
*/
	EVENT_GAUGE_HIGH_TEMPERATURE,

/** Use this event to register callback for temperature event from Gauge. Li-on
* battery are supposed to be operated in range of 0-40 degrees only. In case the
* temperature varies from above range then it is recommended to stop the charging
* of the battery
*/
	EVENT_GAUGE_LOW_TEMPERATURE,

/** User can set a level. For example if user set value 5, then application will
* receive an interrupt every time when SoC crosses 5 unit mark. ie, interrupt will
* be triggeren on 5, 10, 15, 20, .... 90, 95, 100
*/
	EVENT_GAUGE_SOC_DELTA,

/** The 9axis sensor will generate trigger based on motion is any direction
* of the device.
*/
	EVENT_9AXIS_SNR_ANY_MOTION_INT,

/** The RTC device can generated interrupt based on time. To set the interrupt
* user needs to first set the time on which interrupt is required and then
* enable the Alarm interrupt. User can use this event to register callback for
* setting alarm event
*/
	EVENT_RTC_INT,

} E_EVENTS;

/*!
* @breif This eNUM lists charger state
*/
typedef enum {
	E_POWER_NOT_GOOD=0,
	E_POWER_GOOD=4,
	E_CHG_SRC_NO_INPUT=0,
	E_CHG_SRC_USB_HOST_SDP=32,
	E_CHG_SRC_USB_CDP=64,
	E_CHG_SRC_USB_DCP=96,
	E_CHG_SRC_UNKNOWN_HOST=160,
	E_CHG_SRC_NON_STANDRARD_ADAPTOR=192,
	E_CHG_SRC_OTG=224,
}E_CHG_STAT;

/*!
* @breif This eNUM lists charger fault
*/
typedef enum {
	E_FAULT_NORMAL=0,
	E_CHRG_FAULT_INPUT_FAULT=16,
	E_CHRG_FAULT_THERMAL_SHUTDOWN=32,
	E_CHRG_FAULT_SAFETY_TIMER_EXPIRE=48,
	E_BAT_FAULT=8,
	E_NTC_FAULT_WARM=2,
	E_NTC_FAULT_COOL=3,
	E_NTC_FAULT_COLD=5,
	E_NTC_FAULT_HOT=6
}E_CHARGER_FAULT;

typedef enum {
	E_CHARGER_ENABLE=0,
	E_CHARGER_DISABLE
}E_CHARGER_STATE;

/*!
* @breif Error codes used in the library
*/
typedef enum {
	E_MEM_ALLOC_FAILED,		/*!< Failed to allocate dynamic memory */
	E_NOT_SUPPORTED,		/*!< Feature not supported by peripheral */
	E_INIT_FAILED,		/*!< Feature not supported by peripheral */
	E_PARAM_ERR,
} E_ERROR_CODES;

/*!
* @brief This eNUM lists all the peripherals
*
*/
typedef enum {
	E_PERI_PMIC,			/*!< Power management IC */
	E_PERI_BATTERY_CHARGER,	/*!< Battery Charger IC */
	E_PERI_BATTERY_GAUGE,	/*!< Fuel gauge IC */
	E_PERI_AUDIO_CODEC,		/*!< TLV Audio codec */
	E_PERI_9AXIS_SENSOR,	/*!< 9-axis sensor */
	E_PERI_INTERNAL_ADC,	/*!< Internal ADC */
	E_PERI_LED,				/*!< LED controller IC */
	E_PERI_LED_CONTROLLER,	/*!< LED controller IC */
	E_PERI_BUTTON,			/*!< Physical Buttons */
	E_PERI_EXTERNAL_ADC,	/*!< PAC1934 ADC module*/
	E_PERI_EXTERNAL_WATCHDOG, /*!< External Watchdog */
	E_PERI_EXTERNAL_RTC,	/*!< External RTC module */
	E_PERI_SERIAL_EEPROM,	/*!< Serial EEPROM IC */
	E_PERI_WIFI,			/*!< Serial EEPROM IC */
	E_PERI_BT,				/*!< Serial EEPROM IC */
	E_PERI_ALL				/*!< Select All above peripherals */
} E_PERIPHERAL;

/*!
* @brief This eNUM lists all available power modes for peripherals
*
*/
typedef enum {
	E_POWERMODE_ACTIVE,		/*!< Active Mode */
	E_POWERMODE_IDLE,		/*!< Sleep/Idle mode */
	E_POWERMODE_POWEROFF,	/*!< Power off mode */
	E_POWERMODE_MAX			/*!< Select All above peripherals */
} E_POWERMODE;

/*!
* @brief This eNUM lists all available health status of peripherals
*
*/
typedef enum {
	E_HEALTH_GOOD,		/*!< Peripheral OK */
	E_HEALTH_BAD,		/*!< Peripherla Not OK */
	E_HEALTH_UNKNOWN,	/*!< Cannot get Health data */
	E_HEALTH_MAX		/*!< Max Level. For internal use only */
} E_PERI_HEALTH;

/*!
* @brief This eNUM lists all available health status of peripherals
*
*/
typedef enum {
	E_CHARGER_SRC_USB,		/*!< USB connected from PC */
	E_CHARGER_SRC_POGO,		/*!< Either USB connected from adapter or Pogo pin */
	E_CHARGER_SRC_UNKNOWN,	/*!< Un-identified charger source */
	E_CHARGER_SRC_MAX		/*!< Max Level. For internal use only */
} E_CHARGER_SOURCE;

/*!
* @brief This eNUM lists all available battery connection status
*
*/
typedef enum {
	E_BAT_CONN_CONNECTED,	/*!< Battery is connected */
	E_BAT_CONN_NOTCONNECTED,/*!< Battery is not connected */
	E_BAT_CON_UNKNOWN,		/*!< Unknown battery status */
} E_BATTERY_CONNECT_STATUS;

/*!
* @brief This eNUM lists all available charge state
*
*/
typedef enum {
	E_CHARGING_STATE_CHARGING,		/*!< Battery is charging */
	E_CHARGING_STATE_NOTCHARGING,	/*!< Battery is not charging */
	E_CHARGING_STATE_DISCHARGING,	/*!< Battery is discharging */
	E_CHARGING_STATE_FULLYCHARGED,	/*!< Battery is fully charged */
	E_CHARGING_STATE_UNKNOWN		/*!< Unknown battery charging state */
} E_CHARGING_STATE;

/*!
* @brief This eNUM lists all available state of audio jack connection
*
*/
typedef enum {
	E_AUDIO_JACK_CONNECTED,		/*!< Audio Jack is connected */
	E_AUDIO_JACK_NOT_CONNECTED,	/*!< Audio Jack is not connected */
	E_AUDIO_JACK_CONN_UNKNOWN	/*!< Unknown audio jack connection state */
} E_AUDIO_JACK_CONNECTION;

/*!
* @brief This eNUM lists all available state of audio jack connection
*
*/
typedef enum {
	E_9AXIS_SNR_ACCELEROMETER,	/*!< 3 axis of accelerometer */
	E_9AXIS_SNR_GYROSCOPE,		/*!< 3 axis of gyroscope */
	E_9AXIS_SNR_MAGNETOMETER,	/*!< 3 axis of magnetometer */
	E_9AXIS_SNR_ALL_AXES		/*!< 3 axis of magnetometer */
} E_9AXIS_SNR_AXIS;

/*!
* @brief This eNUM lists all available channels of external ADC IC
*
*/
typedef enum {
	E_EXTERNAL_ADC_CHANNEL_3V3_WIFI,	/*!< 3.3V to WiFi LDO */
	E_EXTERNAL_ADC_CHANNEL_1V2,			/*!< 1.2V Processor core supply*/
	E_EXTERNAL_ADC_CHANNEL_3V3,			/*!< 3.3V system and peripheral supply*/
	E_EXTERNAL_ADC_CHANNEL_1V8_3V3,		/*!< 1.8 from external LDO */
	E_INTERNAL_ADC_CHANNEL_LED_FAULT,	/*!< BAD LED detection */
	E_INTERNAL_ADC_CHANNEL_1V35_SENSE,	/*!< Core Voltage sense register */
	E_INTERNAL_ADC_CHANNEL_LM26,		/*!< System temperature control IC */
	E_INTERNAL_ADC_CHANNEL_TEMP_WIFI,	/*!< Thermocouple connected near WiFi */
	E_INTERNAL_ADC_CHANNEL_TEMP_PMIC,	/*!< Thermocouple connected near PMIC */
	E_INTERNAL_ADC_CHANNEL_TEMP_BATTERY /*!< Thermocouple connected near Battery */
} E_ADC_CHANNELS;

/*!
* @brief This eNUM lists all available channels of external ADC IC
*
*/
typedef enum {
	E_LED_OFF,				/*!< LED is off */
	E_LED_COLOR_RED,		/*!< LED color is red */
	E_LED_COLOR_GREEN,		/*!< LED color is green */
	E_LED_COLOR_BLUE,		/*!< LED color is blue */
	E_LED_COLOR_CYAN,		/*!< LED color is cyan = green + blue */
	E_LED_COLOR_YELLOW,		/*!< LED color is yellow = red + green */
	E_LED_COLOR_MAGENTA,	/*!< LED color is magenta = blue + red */
	E_LED_COLOR_WHITE,		/*!< All LED colors glowing */
} E_LED_COLORS;

/*!
* @brief This eNUM lists all available channels of external ADC IC
*
*/
typedef enum {
	E_WATCHDOG_STATE_ENABLED,	/*!< Watchdog is enabled */
	E_WATCHDOG_STATE_DISABLED,	/*!< Watchdog is disabled */
	E_WATCHDOG_STATE_UNKNOWN,	/*!< Watchdog is disabled */
} E_WATCHDOG_STATE;

/*!
* @brief Use this structure to read 9-axis sensor data
*
*/
typedef struct {
	float X_accel; /*!< X axis of accelerometer */
	float Y_accel; /*!< Y axis of accelerometer */
	float Z_accel; /*!< Z axis of accelerometer */
	float X_gyro;  /*!< X axis of gyroscope */
	float Y_gyro;  /*!< Y axis of gyroscope */
	float Z_gyro;  /*!< Z axis of gyroscope */
	float X_magn;  /*!< X axis of magnetometer */
	float Y_magn;  /*!< Y axis of magnetometer */
	float Z_magn;  /*!< Z axis of magnetometer */
} sSensorData;

/*!
* @brief Use this structure to temperature from external temperature
* 	sensor and thermocopules.
*
*/
typedef struct {
	int Temp_LM26; 		/*! System temperature regulator */
	int Temp_WIFI;		/*!< Thermocouple connected near WiFi module */
	int Temp_PMIC;		/*!< Thermocouple connected near PMIC module */
	int Temp_BATTERY;	/*!< Thermocouple connected near Battery */
} sTemperatureData;

/**
* @brief This function is mandatory to be executed before calling others. This
* function allocates resources for internal operation of the library.
*
* @return 0 - Success, 1 - Failure
*/
int InitLibrary (void);

/**
* @brief This function is mandatory to be executed before exiting the application.
* This function fress all resources which are allocated internally by the library.
*
* @return 0 - Success, 1 - Failure
*/
int DeInitLibrary (void);

/**
* @brief Use this function to register callback on files.
*
* @param pVarName Name of the file on which callback has to be added.
* @param FxnCallback Callback function which will be called when there is any
* 	change in the data in the file. Callback function will be called only
* 	when data is written in file.
*	\p The callback function has to in the format of@n
*	"void (*FxnCallback)(CachedValue *, void *)"@n
*	\p CachedValue *p : This data is populated by the internal library.This
*	data can be retrived using different internal function.
*
* @param pCallbackData User data pointer which is shared while registering the
*	callback function
* @param type type defines the type of file on which the callback function is
*	register.@n
*	-# VAR_TYPE_REGISTERED: Use for normal file
*	-# VAR_TYPE_SYSFS: Use for sysfs filesystem
*	-# VAR_TYPE_DEV_INPUT: Use for event nodes
*
* To register callback function for input event, use the enum EVENT2STR which
* will convert the specific event into string. for example to register event
* for audio jack detection use the function as below.
*
*	RegisterCallback (EVENT2STR(EVENT_AUDIO_JACK_DETECT),
*								MyFunc,
*								&data,
*								VAR_TYPE_DEV_INPUT);
*
* @return 1 - Success, 0 - Failure
*/
int RegisterCallback (char *pVarName,
	             void (*FxnCallback)(CachedValue *, void *),
				 void *pCallbackData);

/**
* @brief Use this function to register callback on files.
*
* @param pm_mode Power mode to be set on the peripheral
* @param peripheral Peripheral on which the power mode is to be set
* @see E_POWERMODE
* @see E_PERIPHERAL
*
* @return 0 - Success, 1 - Not Supported, 2 - Failure
*/
int SetPowerMode (int pm_mode, int peripheral);

/**
* @brief Read health status of peripheral
*
* @param health Variable to be shared by application. Health of the peripheral
*	will be populated by the library
* @param peripheral on which the power mode is to be set
* @see E_PERIPHERAL
* @see E_PERI_HEALTH
*
* @return 1 - Success, 0 - Failure
*/
int GetPeipheralHealthStatus (int *health, int peripheral);

/**
* @brief Read the type of source connected for charging. The source is defined
* 	based on the uSB device detection.
*
* @param src Variable shared by application. Connected charger source value will
*	be populated in this variable
*
* @see E_CHARGER_SOURCE
*
* @return 1 - Success, 0 - Failure
*/
int GetBatteryChargerSource (int *src);


/**
* @brief Read the status of battery connection.
*
* @param connect Variable shared by application. Status of battery connection is
*	populated in this variable.
*
* @see E_BATTERY_CONNECT_STATUS
*
* @return 1 - Success, 0 - Failure
*/
int GetBatteryConnectStatus (int *connect);

/**
* @brief Set the state of battery charging. Charging can either be enabled or
*	disabled
*
* @param enable Status set by the application will be executed.
*
* @see E_CHARGING_STATE
*
* @return 1 - Success, 0 - Failure
*/
int SetBatteryChargerState (int enable);

/**
* @brief Read the state of battery charging. Charging can either be enabled or
*	disabled
*
* @param enable Variable shared by application. Status of battery connection is
*	populated in this variable.
*
* @see E_CHARGING_STATE
*
* @return 1 - Success, 0 - Failure
*/
int GetBatteryChargeStatus (int *state);

/**
* @brief Read the current voltage available from the battery
*
* @param voltage Variable shared by application. Battery voltage in units of
*	millivolts will be populated in this variable.
*
* @return 1 - Success, 0 - Failure
*/
int GetBatteryVoltage (int *voltage);

/**
* @brief Read the average current drawn from the battery
*
* @param current Variable shared by application. Average of current drawn
*	from the battery is populated in this variable
*
* @return 1 - Success, 0 - Failure
*/
int GetBatteryCurrent (int *current);

/**
* @brief Read temperature of the battery
*
* @param temp Variable shared by application. Temperature of the battery in
* 	terms of degree celcius will be populated in this variable.
*
* @return 1 - Success, 0 - Failure
*/
int GetBatteryTemperature (int *temp);

/**
* @brief Read the total battery capacity in terms of SoC (State of Charge)
*
* @param soc Variable shared by application. SoC range from 0-100 for
* 	any battery.
*
* @return 1 - Success, 0 - Failure
*/
int GetBatteryCapacity (int *soc);

/**
* @brief Read the estimate time for battery to get fully discharged or till low
* 	battery SoC interrupt occurs
*
* @param time_rem Variable shared by application. Time remaining till complete
* 	battery discharge is populated here. Please note that this data is
*	estimated from the fuel gauge itself.
* @return 1 - Success, 0 - Failure
* @see E_EVENTS
*/
int GetTimeToEmptyInMinutes (int *time_rem);

/**
* @brief Set the volume level of audio capture system.
*
* @param level Volume can be set in levels from 0-100. If the volume is set
*	to 0, the capture interface will be mute.
*
* @return 1 - Success, 0 - Failure
*/
int SetAudioCaptureVolume (int level);

/**
* @brief Set the volume level of audio playback system.
*
* @param level Volume can be set in levels from 0-100. If the volume is set
*	to 0, the playback interface will be mute.

* @return 1 - Success, 0 - Failure
*/
int SetAudioPlaybackVolume(int volume);

/**
* @brief Set the volume level of audio capture amplifier.
*
* @param level Amplifier volume can be set in levels from 0-100.
*
* @return 1 - Success, 0 - Failure
*/
int SetAudioCaptureAmplifierVolume (int level);
int GetAudioCaptureAmplifierVolume (int *level);

/**
* @brief Set the volume level of audio playback amplifier.
*
* @param level Amplifier volume can be set in levels from 0-100.
*
* @return 1 - Success, 0 - Failure
*/
int SetAudioPlaybackAmplifierVolume(int volume);
int GetAudioPlaybackAmplifierVolume(int *volume);


/**
* @brief Read the status of audio jack connection
*
* @param jack_connect Variable shared by application. The status of audio jack
*	connection will be populated in this variable. Jack can be either
*	connected or not connected.
*
* @return 1 - Success, 0 - Failure
* @see	E_AUDIO_JACK_CONNECTION
*/
int GetAudioJackStatus (int *state);


/**
* @brief Read the volume level current set on audio capture module
*
* @param level Volume will be read in levels from 0-100. If the volume is set
*	to 0, the capture interface will be mute.
*
* @return 1 - Success, 0 - Failure
*/
int GetAudioCaptureVolume(int *volume);

/**
* @brief Read the volume level current set on audio playback module
*
* @param level Volume will be read in levels from 0-100. If the volume is set
*	to 0, the playback interface will be mute.
*
* @return 1 - Success, 0 - Failure
*/
int GetAudioPlaybackVolume(int *volume);

/**
* @brief Read the 9-axis sensor data
*
* @param data Variable shared by applicaion. This is pointer to sSensorData
*	varaible.The axis set by axis_sel variable will be populated in this
*	structure. Other axes data will be 0.
* @param axis_sel
* @see sSensorData
* @see E_9AXIS_SNR_AXIS
* @see E_EVENTS
* @return 1 - Success, 0 - Failure
*/
int ReadAllAxesData (sSensorData *data, int axis_sel);

/**
* @brief Set ODR for magnetometer, Accelerometer or Gyroscope
*
* @param odr_to_set-Value of ODR to be set
* @param sensor-Sensor for which ODR is to be set
* @return 1-Success, 0-Failure
*/
int setODR(float odr_to_set, int sensor);

/**
* @brief Read voltage data of any of the 4 channels of external
*	ADC controller IC.
*
* @param channel Set the channel whose data is to be read.
* @param voltage Variable shared by application. The voltage sensed on this
*	channel in unit of milli volts will be populated in this variable
* @see E_ADC_CHANNELS
*
* @return 1 - Success, 0 - Failure
*/
int GetExternalAdcData(int channel, int *voltage, int *current);

/**
* @brief Set the low level threshold of current and voltage for selected
*	channel of external ADC controller IC.
*
* @param channel Set the channel whose data is to be read.
* @param voltage Low level of voltage in units of millivolt
* @param current Low level of current in units of milliampere
* @see E_ADC_CHANNELS
*
* @return 1 - Success, 0 - Failure
*/
int SetInputthresholdLow (int channel, int voltage, int current);

/**
* @brief Set the high level threshold of current and voltage for selected
*	channel of external ADC controller IC.
*
* @param channel Set the channel whose data is to be read.
* @param voltage High level of voltage in units of millivolt
* @param current High level of current in units of milliampere
* @see E_ADC_CHANNELS
*
* @return 1 - Success, 0 - Failure
*/
int SetInputthresholdHigh (int channel, int voltage, int current);

/**
* @brief Set colour of LED
*
* @param color Set the color to be set on LED
* @param brightness Set the brightness of LED
* @see E_LED_COLORS
*
* @return 1 - Success, 0 - Failure
*/
int SetLedColor (int color, int brightness);

/**
* @brief Read the colour set on LED
*
* @param color Set the color currently set on LED
* @see E_LED_COLORS
*
* @return 1 - Success, 0 - Failure
*/
int GetLedColor (int *color);

/**
* @brief Blink LED
*
* @param color Set the color to be set on LED
* @param duration_on Duration in milli seconds for which the led will be
*	turned on
* @param duration_off Duration in milli seconds for which the led will be
*	turned off
*
* @return 1 - Success, 0 - Failure
*/
int SetLedBlink (int color, int duration_on, int duration_off);

/**
* @brief Set the brightness of LED
*
* @param color Color to be turned on
* @param brightness Set level on brightness on LED. Value ranges from
*	0 - 100.
* @see E_LED_COLORS
*
* @return 1 - Success, 0 - Failure
*/
int SetLedBrightness (int color, int brightness);


/**
* @brief Set temperature level on which interrupt needs to be generated
*
* @param temp Temperature in units of degree celcius. In case the system
*	temperature reaches equal to threshold, callback function will be
* 	executed
*
* @return 1 - Success, 0 - Failure
*/
int SetShutdownTemperatureLevel (int src, int level);

/**
* @brief Set the brightness of LED
*
* @param temp Variable shared by application. The temperature threshold
* 	currently set for triggering temperature event will be populated
* 	in this variable
*
* @return 1 - Success, 0 - Failure
*/
int GetShutdownTemperatureLevel (int src, int *level);

/**
* @brief Read the current system temperature
*
* @param data Variable shared by application. Temperature values read from
*	all temperature modules will be populated in this variable
* @see struct sTemperatureData
*
* @return 1 - Success, 0 - Failure
*/
int GetSystemTemperature (void *data);

/**
* @brief Set the state of external watchdog IC
*
* @param state External watchdog can be enabled or disabled.
* @see struct E_WATCHDOG_STATE
*
* @return 1 - Success, 0 - Failure
*/
int SetWatchdogState (int state);

/**
* @brief Read the state of external watchdog IC
*
* @param state External watchdog can be either enabled or disabled.
* @see struct E_WATCHDOG_STATE
*
* @return 1 - Success, 0 - Failure
*/
int GetWatchdogState (int state);

/**
* @brief Set time on External RTC module
*
* @param tTime Standard time struct to be filled by application
*
* @return 1 - Success, 0 - Failure
*/
int SetRtcDateTime (struct tm *tTime);

/**
* @brief Read time from External RTC module
*
* @param tTime Standard time struct filled by internal library
*
* @return 1 - Success, 0 - Failure
*/
int GetRtcDateTime (struct tm *tTime);

/**
* @brief Set alarm for time based interrupt
*
* @param tTime Standard time struct filled by internal library. An interrupt
* 	will be generated when the timer expires. In case the device is in
*	sleep mode, the device will wake up to active mode.
*
* @return 1 - Success, 0 - Failure
*/
int SetAlarm (struct tm *tTime);

/**
* @brief Stop alarm if set.
*
* @return 1 - Success, 0 - Failure
*/
int StopAlarm (void);

/**
* @brief Write data on the external EEPROM
*
* @param data Data that is to be written on external EEPROM
* @param offset Offset from start of EEPROM address
* @param length	Length of the data variable
*
* @return 1 - Success, 0 - Failure
*/
int WriteEeprom(char *data, int offset, int length);

/**
* @brief Read data from the external EEPROM
*
* @param data Data from EEPROM will be writted into this variable
* @param offset Offset from start of EEPROM address
* @param length	Length of data to be read
*
* @return 1 - Success, 0 - Failure
*/
int ReadEeprom (char *data, int offset, int length);

/**
* @brief Erase all contents of EEPROM
*
* @return 1 - Success, 0 - Failure
*/
int EraseEeprom();

/**
* @brief Read the hardware revision of C3 board
*
* @param hw_rev	Hardware revision based on GPIO status. This is permanently
*	set from hardware
*
* @return 1 - Success, 0 - Failure
*/
int ReadHardwareRevision(char *hw_rev);

/**
* @brief Read the registers from I2C peripheral devices
*
* @param peripheral I2C device from which data is to be read
* @param Register Register of the device whose value is to be read
* @param value Value captured from the register
*
* @return 1 - Success, 0 - Failure
*/
int GetI2CDevRegisterValue(int peripheral, int Register, int *value);

/**
* @brief Write registers to I2C peripheral devices
*
* @param peripheral I2C device to which data is to be written
* @param Register Register of the device whose value is to be set
* @param value Value to be set on register
*
*
* @return 1 - Success, 0 - Failure
*/
int SetI2CDevRegisterValue(int peripheral, int Register, int value);

int GetBatteryChargingCurrent(int *current);
int SetBatteryChargingCurrent(int current);

#endif //__LIBINTERFACE_H__
