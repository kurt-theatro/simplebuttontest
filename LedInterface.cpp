#include <stdlib.h>
#include "LibInterface.h"

#define RED_PATH	"/sys/class/leds/Red/brightness"
#define GREEN_PATH	"/sys/class/leds/Green/brightness"
#define BLUE_PATH	"/sys/class/leds/Blue/brightness"

#define ENGINE_SELECT "/sys/bus/i2c/devices/1-0030/select_engine"
#define FIRMWARE_LOADING	"/sys/class/firmware/lp5562/loading"
#define FIRMWARE_DATA	"/sys/class/firmware/lp5562/data"
#define RUN_ENGINE	"/sys/bus/i2c/devices/1-0030/run_engine"

//int ReadRegInt(char *pVarName, int *piNum);
//int WriteRegInt(char *pVarName, int pValue);
//int WriteRegString(char *pVarName, char *pValue);

int SetEngineMux(){
	system("echo RGB > /sys/bus/i2c/devices/1-0030/engine_mux");
	return 1;
}

int TurnOffLed(){

	if(WriteRegInt(RED_PATH, 0)) {
		printf("Unable to turn off Red led\n");
		return 0;
	}
	if(WriteRegInt(GREEN_PATH, 0)){
		printf("Unable to turn off Green led\n");
		return 0;
	}
	if(WriteRegInt(BLUE_PATH, 0)){
		printf("Unable to turn off Blue led\n");
		return 0;
	}
	if(WriteRegInt(RUN_ENGINE, 0)){
		printf("Unable to stop the led engine\n");
			return 0;
	}
	return 1;
}

int SetLedColor (int color, int brightness){

	if(color == E_LED_OFF){
		if(!TurnOffLed()){
			printf("Unable to turn off leds");
			return 0;
		}
	}
	else{
		//Need to turn off previous leds, before switching to new led color.
		if(!TurnOffLed()){
			printf("Unable to turn off leds\n");
			return 0;
		}

		if(!SetLedBrightness(color, brightness)){
			printf("Unable to set Led color\n");
			return 0;
		}
	}

	return 1;
}

int GetLedColor (int *color){

	int red;
	int green;
	int blue;

	//If unable to read current color values, simply return.
	if(ReadRegInt(RED_PATH, &red)){
		printf("unable to read Red led status\n");
		return 0;
	}

	if(ReadRegInt(GREEN_PATH, &green)){
		printf("Unable to read Green led status\n");
		return 0;
	}

	if(ReadRegInt(BLUE_PATH, &blue)){
		printf("Unable to read Blue led status\n");
		return 0;
	}

	if(red > 0 && green > 0 && blue > 0)
		*color =  E_LED_COLOR_WHITE;
	else if(blue > 0 && red > 0)
		*color = E_LED_COLOR_MAGENTA;
	else if(green > 0 && red > 0)
		*color = E_LED_COLOR_YELLOW;
	else if(green > 0 && blue > 0)
		*color = E_LED_COLOR_CYAN;
	else if(red > 0)
		*color = E_LED_COLOR_RED;
	else if(green > 0)
		*color = E_LED_COLOR_GREEN;
	else if(blue > 0)
		*color = E_LED_COLOR_BLUE;

	return 1;
}

int SetLedBrightness (int color, int brightness){

	SetEngineMux();
	switch(color)
	{
		case E_LED_COLOR_RED:
			if(WriteRegInt(RED_PATH, brightness)){
				printf("Unable to set brightness of red led\n");
				return 0;
			}
			break;
		case E_LED_COLOR_GREEN:
			if(WriteRegInt(GREEN_PATH, brightness)){
				printf("Unable to set brightness of green led\n");
				return 0;
			}
			break;
		case E_LED_COLOR_BLUE:
			if(WriteRegInt(BLUE_PATH, brightness)){
				printf("Unable to set brightness of blue led\n");
				return 0;
			}
			break;
		case E_LED_COLOR_CYAN:
			if(WriteRegInt(GREEN_PATH, brightness)){
				printf("Unable to turn on Green led\n");
				return 0;
			}
			if(WriteRegInt(BLUE_PATH, brightness)){
				printf("Unable to turn on Blue led\n");
				return 0;
			}
			break;
		case E_LED_COLOR_YELLOW:
			if(WriteRegInt(RED_PATH, brightness)){
				printf("Unable to turn on Red led\n");
				return 0;
			}
			if(WriteRegInt(GREEN_PATH, brightness)){
				printf("Unable to turn on Green led\n");
				return 0;
			}
			break;
		case E_LED_COLOR_MAGENTA:
			if(WriteRegInt(BLUE_PATH, brightness)){
				printf("Unable to turn on Blue led\n");
				return 0;
			}
			if(WriteRegInt(RED_PATH, brightness)){
				printf("Unable to turn on Red led\n");
				return 0;
			}
			break;
		case E_LED_COLOR_WHITE:
			if(WriteRegInt(GREEN_PATH, brightness)){
				printf("Unable to turn on Green led\n");
				return 0;
			}
			if(WriteRegInt(BLUE_PATH, brightness)){
				printf("Unable to turn on Blue led\n");
				return 0;
			}
			if(WriteRegInt(RED_PATH, brightness)){
				printf("Unable to turn on Red led\n");
				return 0;
			}
			break;
		default:
			printf("Wrong set-color brightness request\n");
			return 0;
	}
	return 1;
}

int FormBlinkData(int duration_on, int duration_off, char* data){

	int interval_on = (int)(duration_on / 998);
	int step_size_on = duration_on / ( 15.6 * (interval_on +1));
	unsigned short data_on = 0x4000 | (step_size_on << 8 ) | (interval_on);
	sprintf(data,"%s%x",data,0x4000);

	int interval_off = (int)(duration_off / 998);
	int step_size_off = duration_off / ( 15.6 * (interval_off +1));
	unsigned short data_off = 0x4000 | (step_size_off << 8 ) | (interval_off);
	sprintf(data,"%s%x",data,data_off);

	sprintf(data,"%s%x",data,0x40FF);
	sprintf(data, "%s%x",data,data_on);
	sprintf(data, "%s0000%x",data,0xD800);

	return 1;
}

int BlinkRedLed(int color, int duration_on, int duration_off){

	char data[25];
	memset(data, 0, sizeof(data));

	SetEngineMux();
	sleep(0.5);
	if(WriteRegInt(ENGINE_SELECT, 1)){
		printf("Unable to select engine\n");
		return 0;
	}
	sleep(1);
	if(WriteRegInt(FIRMWARE_LOADING, 1)){
		printf("Unable to load the firmware\n");
		return 0;
	}

	FormBlinkData(duration_on, duration_off, data);

	if(WriteRegString(FIRMWARE_DATA, data)){
		printf("Unable to set firmware data\n");
		return 0;
	}
	sleep(0.05);
	if(WriteRegInt(FIRMWARE_LOADING, 0)){
		printf("Unable to load the firmware\n");
		return 0;
	}

	return 1;
}

int BlinkGreenLed(int color, int duration_on, int duration_off){

	char data[25];
	memset(data, 0, sizeof(data));

	SetEngineMux();
	sleep(0.5);
	if(WriteRegInt(ENGINE_SELECT, 2)){
		printf("Unable to select engine\n");
		return 0;
	}
	sleep(1);
	if(WriteRegInt(FIRMWARE_LOADING, 1)){
		printf("Unable to load the firmware\n");
		return 0;
	}

	FormBlinkData(duration_on, duration_off, data);

	if(WriteRegString(FIRMWARE_DATA, data)){
		printf("Unable to set firmware data\n");
		return 0;
	}

	sleep(0.05);
	if(WriteRegInt(FIRMWARE_LOADING, 0)){
		printf("Unable to load the firmware\n");
		return 0;
	}

	return 1;
}

int BlinkBlueLed(int color, int duration_on, int duration_off){

	char data[25];
	memset(data, 0, sizeof(data));

	SetEngineMux();
	sleep(0.5);
	if(WriteRegInt(ENGINE_SELECT, 3)){
		printf("Unable to select engine\n");
		return 0;
	}
	sleep(1);
	if(WriteRegInt(FIRMWARE_LOADING, 1)){
		printf("Unable to load the firmware\n");
		return 0;
	}

	FormBlinkData(duration_on, duration_off, data);

	if(WriteRegString(FIRMWARE_DATA, data)){
		printf("Unable to set firmware data\n");
		return 0;
	}
	sleep(0.05);
	if(WriteRegInt(FIRMWARE_LOADING, 0)){
		printf("Unable to load the firmware\n");
		return 0;
	}

	return 1;
}

int SetLedBlink (int color, int duration_on, int duration_off){

	//First turn off the current Leds.
	if(!TurnOffLed()){
			printf("Unable to turn off leds\n");
			return 0;
	}

	switch(color){
		case E_LED_COLOR_RED:
			if(!BlinkRedLed(color, duration_on, duration_off))
				return 0;
			break;
		case E_LED_COLOR_GREEN:
			if(!BlinkGreenLed(color, duration_on, duration_off))
				return 0;
			break;
		case E_LED_COLOR_BLUE:
			if(!BlinkBlueLed(color, duration_on, duration_off))
				return 0;
			break;
		case E_LED_COLOR_CYAN:
			if(!BlinkGreenLed(color, duration_on, duration_off))
				return 0;
			if(!BlinkBlueLed(color, duration_on, duration_off))
				return 0;
			break;
		case E_LED_COLOR_YELLOW:
			if(!BlinkRedLed(color, duration_on, duration_off))
				return 0;
			if(!BlinkGreenLed(color, duration_on, duration_off))
				return 0;
			break;
		case E_LED_COLOR_MAGENTA:
			if(!BlinkBlueLed(color, duration_on, duration_off))
				return 0;
			if(!BlinkRedLed(color, duration_on, duration_off))
				return 0;
			break;
		case E_LED_COLOR_WHITE:
			if(!BlinkBlueLed(color, duration_on, duration_off))
				return 0;
			if(!BlinkRedLed(color, duration_on, duration_off))
				return 0;
			if(!BlinkGreenLed(color, duration_on, duration_off))
				return 0;
			break;
		default:
			printf("Wrong request, Unable to blink led!\n");
			return 0;
	}
	//Run the engine.
	sleep(1);
	if(WriteRegInt(RUN_ENGINE, 1)){
		printf("Unable to run the led engine\n");
		return 0;
	}
	return 1;
}
