DIAGPROG = tester

# For pure Linux, use these settings for CPP/CC
TOOL_DIR=/home/theatro/ravi/compile/SDK

CROSS_COMPILE=$(TOOL_DIR)/toolchain/poky-atmel/2.5.1/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi/arm-poky-linux-gnueabi-
export CPP=$(CROSS_COMPILE)g++ -march=armv7-a -marm -mfpu=neon -mfloat-abi=hard -mcpu=cortex-a5 --sysroot=/opt/poky-atmel/2.5.1/sysroots/cortexa5hf-neon-poky-linux-gnueabi

SVNREV := -D'SVNREVISION="19048"'

CPPFLAGS =  $(SVNREV) -g -w -Wshadow -Wpointer-arith -Wcast-qual -Winline -MMD -fPIC -fpermissive -pthread -lrt -DLINUX -lasound -g

DIAGCPPSOURCES = \
	Common.cpp \
	LedInterface.cpp \
	LibInterface.cpp \
	ButtonTest.cpp \
	Utils.cpp 
	

all: tester 


tester: 
	
	$(CPP) $(DIAGCPPSOURCES) $(CPPFLAGS) -o $(DIAGPROG)


.PHONY: clean

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ 
	rm -f tester
 

