#include <istream>
#include <fstream>
#include <sstream>
#include <streambuf>
#include <iostream>
#include <stdexcept>
#include <cstddef>
#include <algorithm>

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/time.h>
#include <poll.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <strings.h>
#include <pthread.h>
#include <unistd.h>
#include "Utils.h"
#include "LibInterface.h"



using namespace std;

std::string macAddress = "";



// System call. Abstract to function in case different platforms clear differently 
void clearScreen() {
    system("clear");
}



// STUBBED FOR NOW - RETURN RANDOM TRUE/FALSE
bool readLEDSetResult() {
  bool randbool = rand() & 1;
//  return randbool;
  return true;
}


void upcaseStr(char *strParam) {
  while (*strParam) {
    *strParam = toupper((unsigned char) *strParam);
    strParam++;
  }
}


const char* testResultToString(testResult result)
{
    switch (result) {
        case pass:   return PASS;
        case fail:   return FAIL;
        case running: return RUNNING;
        case pending: return PENDING;
    }
}

char* stoupper( char* s )  {
  char* p = s;
  while (*p = toupper( *p )) p++;
  return s;
}


void powerDown() {

   cout << "calling /sbin/poweroff" << endl;

}




