#include "LibInterface.h"

#define MAX_PATH_LEN	(255)
#define SYSTEM_OK	0
#define SYSTEM_ERROR 1

char *EVENT2STR [] ={
	"/dev/input/by-path/platform-gpio_keys-event^1^257",    //EVENT_BUTTON_COMMAND_BUTTON,
	"/dev/input/by-path/platform-gpio_keys-event^1^258",    //EVENT_BUTTON_VOLUME_PLUS,,
	"/dev/input/by-path/platform-gpio_keys-event^1^259",    //EVENT_BUTTON_VOLUME_MINUS,
	"/dev/input/by-path/platform-gpio_keys-event^1^260",    //EVENT_BUTTON_PTT,
	"/dev/input/by-path/platform-sound-event^5^2",          //EVENT_AUDIO_JACK_DETECT,
	"/dev/input/event0^4^3",//EVENT_CHARGER_SRC_CONNECT,
	"/dev/input/event0^4^2",//EVENT_CHARGE_STATUS,
	"/dev/input/event0^4^4",//EVENT_CHARGE_POWERGOOD,
	"/dev/input/event0^4^1",//EVENT_CHARGE_FAULT,
	"/dev/input/event0^4^0",//EVENT_BATTERY_FAULT,
	"/dev/input/event0^4^5",//EVENT_NTC_FAULT,
	"",//EVENT_GAUGE_LOW_SOC,
	"",//EVENT_GAUGE_HIGH_TEMPERATURE,
	"",//EVENT_GAUGE_LOW_TEMPERATURE,
	"/dev/input/event1^4^4",                                //EVENT_GAUGE_SOC_DELTA,
	"",//EVENT_9AXIS_SNR_ANY_MOTION_INT,
	"",//EVENT_RTC_INT,
};

int isFileExist(char *fname)
{
	int status = -1;
	if( access( fname, F_OK ) != -1 ) {
		status = 0;
	}

	return status;
}

int ReadRegString(char *pVarName, char *pValue, int len) {
    FILE *fp;
    char sTempVar[MAX_PATH_LEN];
    memset(sTempVar, 0x0, MAX_PATH_LEN);
    fp = fopen(pVarName, "rb");
    if (fp) {

        fscanf(fp, "%256[^\n]", sTempVar);
		snprintf(pValue, len, "%s", sTempVar);
        fclose(fp);
    } else {
		printf("Error in reading %s [%s]", pVarName, strerror(errno));
		return -1;
	}

    return 0;

}

void ReadRegStringWithSpace(char *pVarName, char *pValue)
{
	FILE *fp;
	memset(pValue, 0, MAX_PATH_LEN);
	fp = fopen(pVarName, "rb");
	if (fp)
	{
		fscanf(fp, "%255[^\n]", pValue);
		fclose(fp);
	}

	return;
}


int ReadRegFloat(char *pVarName, float *pfNum)
{
    FILE *fp;
	float value = 0;

    fp = fopen(pVarName, "rb");
    if (fp) {
        fscanf(fp, "%f", &value);
		*pfNum = value;
        //printf("Float value: %f, %s\n", *pfNum, pVarName);
        fclose(fp);
    } else {
		printf("Error in reading %s [%s].", pVarName, strerror(errno));
		return -1;
	}

    return 0;
}


/**
** Function that reads an intergral value from a registry file
**
** @param[in] pVarName Name of registry file
** @param[out] piNum value
** @return nothing
**/

int ReadRegInt(char *pVarName, int *piNum)
{
    FILE *fp;
	int value = 0;

    fp = fopen(pVarName, "rb");
    if (fp) {
        fscanf(fp, "%d", &value);
		*piNum = value;
        //printf("Integer value: %d, file : %s\n", *piNum, pVarName);
        fclose(fp);
    } else {
		printf("Error in reading %s [%s].", pVarName, strerror(errno));
		return -1;
	}

    return 0;
}
int ReadRegHex(char *pVarName, int *piNum)
{
    FILE *fp;
    int iStatus = 0;
    fp = fopen(pVarName, "rb");
    if (fp)
    {
        fscanf(fp, "%x", piNum);
        printf("Hex value: %d\n", *piNum);
        fclose(fp);
    }
    else
    {
		printf("Error in reading %s [%s].", pVarName, strerror(errno));
        iStatus = SYSTEM_ERROR;
    }

    return iStatus;
}

int WriteRegInt(char *pVarName, int pValue)
{
	FILE *fp;
	fp = fopen(pVarName, "wb");
	if (fp) {
		if(fprintf(fp, "%d", pValue)) {
			//printf("written %d to %s\n", pValue, pVarName);
		} else {
			printf("%s: Unable to write [%s]\n", __func__, strerror(errno));
		}

		fclose(fp);
	} else {
		printf("Unable to open %s [%s]\n", pVarName, strerror(errno));
		return SYSTEM_ERROR;
	}

	return SYSTEM_OK;
}

int WriteRegString(char *pVarName, char *pValue)
{
	FILE *fp;

	if(!pValue || !pVarName) {
		if(pValue)
			//printf("%s: pVarName Null argument. pValue:%s\n", __func__, pValue);
		if(pVarName)
			//printf("%s: pValue Null argument. pVarName:%s\n", __func__, pVarName);
		return SYSTEM_ERROR;
	}

	fp = fopen(pVarName, "wb");
	if (fp) {
		if (fprintf(fp, "%s", pValue)) {
			//printf("Written %s to %s\n",pValue, pVarName);
		} else {
			printf("%s: Unable to write [%s]\n", __func__, strerror(errno));
		}
		fclose(fp);
	} else {
		printf("Unable to open %s [%s]\n", pVarName, strerror(errno));
		return SYSTEM_ERROR;
	}

	return SYSTEM_OK;
}

int WriteRegFloat(char *pVarName, float pValue)
{
	FILE *fp;
	fp = fopen(pVarName, "wb");
	if (fp) {
		fprintf(fp, "%f", pValue);
		fclose(fp);
	}

	return 0;
}

int CreateDirectory(char *path)
{
	int iRetVal                 = SYSTEM_OK;
	int iStatus                 = 0;
	char strPath[MAX_PATH_LEN]  = {0};
	char strRegVar[MAX_PATH_LEN]= {0};
	char *pStr                  = NULL;
	char *saveptr               = NULL;
	struct stat stat_buf;

	if (path == NULL) {
		printf("Invalid arguments\n");
		return iRetVal;
	}

	iStatus = stat(path, &stat_buf);
	if (iStatus == 0) {
		printf("Directory <%s> already present", path);
		return SYSTEM_OK;
	}

	snprintf(strPath, MAX_PATH_LEN, "%s", path);
	memset(strRegVar, 0x0, sizeof(strRegVar));

	pStr = strtok_r(strPath, "/", &saveptr);
	while (pStr != NULL) {
		strcat(strRegVar, "/");
		strcat(strRegVar, pStr);
		printf("Creating directory <%s>", strRegVar);
		/* Check if file/directory already exists */
		iStatus = stat(strRegVar, &stat_buf);
		if (iStatus == 0) {
			printf("Directory <%s> already present", strRegVar);
			pStr = strtok_r(NULL, "/", &saveptr);
			continue;
		}

		iStatus = mkdir(strRegVar, 0755);
		if (iStatus != 0 && errno != EEXIST) {
			printf("Failed to create directory <%s :: %s>, %s", path, strRegVar, strerror(errno));
			iRetVal = SYSTEM_ERROR;
			break;
		}

		pStr = strtok_r(NULL, "/", &saveptr);
	}

	if (iRetVal != SYSTEM_ERROR) {
		if (pStr == NULL) {
			iRetVal = SYSTEM_OK;
		}
	}

	return iRetVal;
}
