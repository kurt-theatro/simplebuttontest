#include <istream>
#include <iostream>
#include <stdlib.h>
#include <cstddef>
#include <string>
#include <getopt.h>
#include <cctype>
#include <map>
#include "LibInterface.h"
#include "Utils.h"
#include <stdio.h>
#include "ButtonTest.h"
#include <semaphore.h>
#include <signal.h>


using namespace std;

// Give user ability to leave test early if they hit control C
bool buttonTestCntlC = false; 

typedef std::tuple<string,int,int> buttonTestResult;

buttonTestResult buttonTestArray[BUTTONS_TO_TEST] = {
   std::make_tuple("Command Button     ",0,0) ,
   std::make_tuple("Broadcast Button   ",0,0) ,
   std::make_tuple("Volume Down Button ",0,0) ,
   std::make_tuple("Volume Up Button   ",0,0) 
};


void handleExitButtonTest() {
    cout << "\nExiting button test" << endl;
    DeInitLibrary (); 
    buttonTestCntlC = true;
}


void buttonTestINThandler(int sig)
{
     signal(sig, SIG_IGN);
     handleExitButtonTest();
}


void performPrimaryButtonAction(bool isDown) {

  if (isDown) {
       SetLedColor(E_LED_COLOR_RED, 0);
    } else {
       SetLedColor(E_LED_COLOR_RED, MAX_LED_BRIGHTNESS);
    }    

}

void performBroadcastButtonAction(bool isDown) {
   if (isDown) {
        SetLedColor(E_LED_COLOR_GREEN, MAX_LED_BRIGHTNESS);
    } else {
        SetLedColor(E_LED_COLOR_GREEN, 0);
    }
}


void performVolumeUpButtonAction(bool isDown) {
   
   if (isDown) {
        SetLedColor(E_LED_COLOR_MAGENTA, MAX_LED_BRIGHTNESS);
    } else {
        SetLedColor(E_LED_COLOR_GREEN, 0);
    }
}


void performVolumeDownButtonAction(bool isDown) {
   if (isDown) {
        SetLedColor(E_LED_COLOR_BLUE,MAX_LED_BRIGHTNESS);
    } else {
        SetLedColor(E_LED_COLOR_BLUE, 0);
    }
}


void CallBackCmdButton(CachedValue *pCachedVal, void *pData)
{
	//printf("%s called with value=%d\n",__func__, pCachedVal->iValue);
	if(pCachedVal->iValue > 0)
        {
           *(int *) pData = 1 ;
            performPrimaryButtonAction(0);
        }
	else if(pCachedVal->iValue == 0) {
           *(int *) pData = 0 ;
            performPrimaryButtonAction(1);
        }
	return;
}

void CallBackVolPlusButton(CachedValue *pCachedVal, void *pData)
{
	//printf("%s called with value=%d\n",__func__, pCachedVal->iValue);
	if(pCachedVal->iValue > 0) {
           *(int *) pData = 1 ;
           performVolumeUpButtonAction(1);
        }
	else if(pCachedVal->iValue == 0) {
           *(int *) pData = 0 ;
            performVolumeUpButtonAction(0);
        }
	return;
}


void CallBackPTTButton(CachedValue *pCachedVal, void *pData)
{
	//printf("%s called with value=%d\n",__func__, pCachedVal->iValue);
	if(pCachedVal->iValue > 0)
        {
           *(int *) pData = 1 ;
  	   performBroadcastButtonAction(1);
        }
	else if(pCachedVal->iValue == 0) {
           *(int *) pData = 0 ;
            performBroadcastButtonAction(0);
        }
	return;
}


void CallBackVolMinusButton(CachedValue *pCachedVal, void *pData)
{
	if(pCachedVal->iValue > 0) {
           *(int *) pData = 1 ;
           performVolumeDownButtonAction(1);
        }
	else if(pCachedVal->iValue == 0) {
           *(int *) pData = 0 ;
           performVolumeDownButtonAction(0);
        }

	return;
}


void registerButtonCallbackFunction(char *pVarName, void (*FxnCallback)(CachedValue *, void *), void *data) {

  int ret = RegisterCallback (pVarName, FxnCallback, data);
  if (ret == -1) {
     cout << "Failed to init interface library" << endl; 
  }

}

void displayButtonClicks(buttonTestResult *buttonTest) {
     clearScreen(); 
    char state[80];

    for (int buttonIndex =0; buttonIndex < 4; buttonIndex++) {
       string name =  std::get<BUTTON_TO_TEST>(buttonTest[buttonIndex]);
       int clkResult =  std::get<CURR_CLICK_CNT>(buttonTest[buttonIndex]);
       if (clkResult)
         strcpy(state , "PRESSED");
       else
         strcpy(state, "NOT PRESSED");
       cout << name <<  " :  [" << state <<  "]" << endl;
    }
}


void initButtonTestResults(buttonTestResult *buttonTest) {
   for (int buttonIndex =0; buttonIndex < BUTTONS_TO_TEST; buttonIndex++) {
       std::get<PREV_CLICK_CNT>(*buttonTest) = 0;
       std::get<CURR_CLICK_CNT>(*buttonTest) = 0;
    }
}



void initButtonTest() {
    static bool callbacksRegistered = false;

    initButtonTestResults(buttonTestArray);
    if (!callbacksRegistered) {
       registerButtonCallbackFunction (EVENT2STR[EVENT_BUTTON_COMMAND_BUTTON],
           CallBackCmdButton, &std::get<CURR_CLICK_CNT>(buttonTestArray[COMMAND_BUTTON])); 
       registerButtonCallbackFunction (EVENT2STR[EVENT_BUTTON_VOLUME_PLUS], 
           CallBackVolPlusButton, &std::get<CURR_CLICK_CNT>(buttonTestArray[VOLUME_UP_BUTTON])); 
       registerButtonCallbackFunction (EVENT2STR[EVENT_BUTTON_VOLUME_MINUS],
           CallBackVolMinusButton, &std::get<CURR_CLICK_CNT>(buttonTestArray[VOLUME_DOWN_BUTTON])); 
       registerButtonCallbackFunction (EVENT2STR[EVENT_BUTTON_PTT],
            CallBackPTTButton, &std::get<CURR_CLICK_CNT>(buttonTestArray[BROADCAST_BUTTON])); 
       callbacksRegistered = true;
    } 

     buttonTestCntlC = false; 
}

testResult buttonTestLoop(){
    bool buttonTestNotDone= true; 
    char response[80];

    displayButtonClicks(&buttonTestArray[0]); 
    while (buttonTestNotDone) {
       bool changeDetected=false;
    
       // if any previous conts don't match current counts, update to indicate a display update
       // is needed 
       for (int i =0; i < BUTTONS_TO_TEST; i++) {
           if (std::get<PREV_CLICK_CNT>(buttonTestArray[i]) != std::get<CURR_CLICK_CNT>(buttonTestArray[i])) {
               std::get<PREV_CLICK_CNT>(buttonTestArray[i]) = std::get<CURR_CLICK_CNT>(buttonTestArray[i]);
               changeDetected = true;
           }  
       }

       // Only update UI when change detected to avoid flicker 
       if (changeDetected) {
           displayButtonClicks(buttonTestArray); 
       }

       // if user hit control C exit loop
       if (buttonTestCntlC) 
          buttonTestNotDone = false; 

       sleep(.5);
    }


    return pass;
}


void init() {
  int ret = InitLibrary ();
  initButtonTest();

  signal(SIGINT, buttonTestINThandler);
}


int main(int argc, char * argv[]) {
    init();
    testResult result= buttonTestLoop();
}
