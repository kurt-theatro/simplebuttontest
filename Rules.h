/*******************************************************************************
 * © Copyright 2011 - 2012, Safety Vision, LLC. All Rights Reserved.
 * TRADE SECRETS: CONFIDENTIAL AND PROPRIETARY
 * This software, in all of its forms, and all of the code, algorithms,
 * formulas, techniques and processes embedded therein, and their structure,
 * sequence, selection and arrangement (the "Software") is owned by and
 * constitutes the valuable work, property and trade secrets of  Safety Vision, LLC
 * or its licensors ("Safety Vision"), is protected by copyright law,
 * trade secret law, and other intellectual property laws, treaties,
 * conventions and agreements, including the applicable laws of the country
 * in which it is being used, and any use, copying, revision, adaptation,
 * distribution or disclosure in whole or in part is strictly prohibited
 * except under the terms of an express written consent and license from
 * Safety Vision, LLC.  The Software is available under a license only, and all
 * ownership and other rights are retained by  Safety Vision, LLC.
 ******************************************************************************/
#ifndef RULES_H_
#define RULES_H_

#include <time.h>
#include <pthread.h>

#define TRUE 1
#define FALSE 0

#define SUCCESS TRUE
#define FAILURE FALSE

#define CACHEDVALUE CachedValue*
#define TABLE Table*
#define CALLBACK Callback*

#define BROKENVARID -1
#define BROKENRULEID -1
#define BROKENREGISTRATION -1

#define NOTBROKENVAR(var) ((var) != BROKENVARID)

typedef struct _type{
	unsigned type;
} Type;

typedef struct _timedata {
	time_t time;
	struct tm tm;
} TimeData;

typedef struct _cachedvalue {
	Type Type1;
	int fValid;
	char* pString;
	int strBufferSize;
	TimeData Time;
	float fValue;
	int iValue;
    unsigned event_mask;
} CachedValue;

#define VALID_NONE		CACHE_TYPE_NONE
#define VALID_INT		CACHE_TYPE_INT
#define VALID_FLOAT		CACHE_TYPE_FLOAT
#define VALID_STRING	CACHE_TYPE_STRING
#define VALID_TIME		CACHE_TYPE_TIME

#define INTVALID(var) (((CachedValue*)(var))->fValid & VALID_INT)
#define FLOATVALID(var) (((CachedValue*)(var))->fValid & VALID_FLOAT)
#define STRINGVALID(var) (((CachedValue*)(var))->fValid & VALID_STRING)
#define TIMEVALID(var) (((CachedValue*)(var))->fValid & VALID_TIME)

typedef struct _regcallback {
	void (*CB)(CachedValue*, void*);
}RegCallback;

typedef struct _callback {
	Type Type1;
	union {
		void* Function;
		RegCallback Registered;
	} Style;
	void* pData;
	struct _callback* pNext;
	struct _callback* pPrev; // double linked for easy removal
} Callback;

typedef struct _commonentry {
	Type VarType;
	int RegistrationID;
	char* pVarString;
	CachedValue* pCachedValue;
	struct _table* pTable;
	Callback* pFirstCallback;
	pthread_mutex_t mutex;
	void* pNext;
	int event;
} TableEntry;

typedef struct _pipeparts {
	int ReadFD;
	int WriteFD;
} PipeParts;

typedef struct _pipefds {
	int pfd[2];
} PipeFDS;

typedef union _pipestruct {
	PipeFDS Descriptors;
	PipeParts Parts;
} PipeStruct;

typedef struct _exceptionFDentry {
	volatile int Descriptor;
	int SkipCallback;
	int RegistrationID;
	void *next;
} ExceptionFDEntry;

typedef struct {
	volatile int Descriptor;
	char *filename;
	char *sDeviceNodeName;
	int RegistrationID;
	int type;
	int code;
	void *next;
} EventFDEntry;

typedef struct _inotify {
	volatile int Descriptor; // iNotify descriptor returned from inotify_init()
	pthread_t Thread; // TID of thread that watches for file and directory changes
	int ThreadStarted; // indicator that thread was started (0 == started)
	PipeStruct DeathPipe; // write to this pipe to kill thread
	int NumberRegistered; // total number of files and directories registered
} iNotifyPart;

typedef struct _table {
	Type Type1;
	TableEntry* pTableEntries; // chain of variable table entries
	pthread_mutex_t mutex;
	iNotifyPart Notify;
	int ExFdNumberRegistered;
	int EvtFdNumberRegistered;
	ExceptionFDEntry *pExFdentry; // Exceptional file descriptor Entry
	EventFDEntry *pEvtFdentry;  // Event file descriptor Entry
} Table;

// structure types
#define TYPE_NO_TYPE		0 // uninitialized value
#define TYPE_EXPRESSION		1
#define TYPE_AND			2
#define TYPE_OR				3

#define TYPE_NOT			4

#define TYPE_NOOP			0x10
#define TYPE_IF				0x11
#define TYPE_NOTIFICATION	0x12

#define TYPE_SET			0x20
#define TYPE_RUN			0x21

#define VAL_TYPE_INT		0x31
#define VAL_TYPE_FLOAT		0x32
#define VAL_TYPE_STRING		0x33
#define VAL_TYPE_TIMESPEC	0x34

#define VAR_TYPE_LOCAL		0x40
#define VAR_TYPE_REGISTERED	0x41
#define VAR_TYPE_CONSTANT	0x42
#define VAR_TYPE_AGGREGATE	0x43
#define VAR_TYPE_RWREGISTERED	0x44
#define VAR_TYPE_SYSFS		0x45
#define VAR_TYPE_EVT		0x46

#define TYPE_CACHED_DATA	0x50

#define TYPE_TABLE			0x60

#define TAG_TYPE_THEN		0x70
#define TAG_TYPE_ELSE		0x71

#define TAG_TYPE_RULES		0x80

#define TYPE_CALLBACK		0x90

#define TOTABLE(var) ((TableEntry*)(var))

#define TYPE(var) ((var)->Type1.type)

#define ISRUN(var) (TYPE(var) == TYPE_RUN)
#define ISSET(var) (TYPE(var) == TYPE_SET)
#define ISEXPRESSION(var) (TYPE(var) == TYPE_EXPRESSION)
#define ISAND(var) (TYPE(var) == TYPE_AND)
#define ISOR(var) (TYPE(var) == TYPE_OR)
#define ISNOT(var) (TYPE(var) == TYPE_NOT)
#define ISIF(var) (TYPE(var) == TYPE_IF)
#define ISTHEN(var) (TYPE(var) == TYPE_THEN)
#define ISELSE(var) (TYPE(var) == TYPE_ELSE)
#define ISNOTIFICATION(var) (TYPE(var) == TYPE_NOTIFICATION)
#define ISCACHE(var) (TYPE(var) == TYPE_CACHED_DATA)
#define ISTABLE(var) (TYPE(var) == TYPE_TABLE)
#define ISCALLBACK(var) (TYPE(var) == TYPE_CALLBACK)

#define VARTYPE(var) (TOTABLE(var)->VarType.type)

#define ISAGG(var) (VARTYPE(var) == VAR_TYPE_AGGREGATE)
#define ISLOCAL(var) (VARTYPE(var) == VAR_TYPE_LOCAL)
#define ISREGISTERED(var) (VARTYPE(var) == VAR_TYPE_REGISTERED)
#define ISRWREGISTERED(var) (VARTYPE(var) == VAR_TYPE_RWREGISTERED)
#define ISCONST(var) (VARTYPE(var) == VAR_TYPE_CONSTANT)
#define ISSYSFS(var) (VARTYPE(var) == VAR_TYPE_SYSFS)
#define ISEVT(var) (VARTYPE(var) == VAR_TYPE_EVT)

#define HASRULES(var) (ISLOCAL(var) || ISREGISTERED(var) || ISAGG(var))
#define HASCACHE(var) (ISLOCAL(var) || ISREGISTERED(var) || ISCONST(var) || ISSYSFS(var))
#define HASREGISTRATION(var) (ISREGISTERED(var) || ISAGG(var) || ISSYSFS(var))
#define HASBROKENREGISTRATION(var) (TOTABLE(var)->RegistrationID == BROKENREGISTRATION)
#define ISVAR(var) (ISLOCAL(var) || ISREGISTERED(var) || ISRWREGISTERED(var) || ISSYSFS(var))


// comparison operations
#define OP_NO_OP			TYPE_NO_TYPE // uninitialized value
#define OP_EQUAL			1
#define OP_NOT_EQUAL		2
#define OP_GREATER			3
#define OP_LESS				4
#define OP_GREATER_EQUAL	5
#define OP_LESS_EQUAL		6

// attributes
#define ATTR_NO_OP			TYPE_NO_TYPE // uninitialized value
#define ATTR_VAR			1
#define ATTR_LOCALVAR		2
#define ATTR_AGGREGATE		3
#define ATTR_SCRIPT			4
#define ATTR_OP				5

// clean up any orphan rules in the variable tables
void ValidateRuleTable(Table* pTable);

// table value should be updated before calling this
int ExecuteRules(TableEntry* pTE);

// set a variable from a cached value
int SetVariableValue(TableEntry *pVar, CachedValue* pCache);

int isFileExist(char *fname);
int ReadRegString(char *pVarName, char *pValue, int len);
void ReadRegStringWithSpace(char *pVarName, char *pValue);
int ReadRegFloat(char *pVarName, float *pfNum);
int ReadRegInt(char *pVarName, int *piNum);
int ReadRegHex(char *pVarName, int *piNum);
int WriteRegInt(char *pVarName, int pValue);
int WriteRegString(char *pVarName, char *pValue);
int WriteRegFloat(char *pVarName, float pValue);
int CreateDirectory(char *path);
#endif /*RULES_H_*/
