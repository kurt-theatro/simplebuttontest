#ifndef BUTTON_TEST_H_
#define BUTTON_TEST_H_  
#include "Utils.h"


testResult runButtonTest(char *tgsServerIP);

#define BUTTONS_TO_TEST    4

#define COMMAND_BUTTON     0 
#define BROADCAST_BUTTON   1
#define VOLUME_DOWN_BUTTON 2
#define VOLUME_UP_BUTTON   3

#define BUTTON_TO_TEST     0
#define PREV_CLICK_CNT     1
#define CURR_CLICK_CNT     2
#define CLICK_RESULT       3
#define LED_RESULT         4 


#endif


