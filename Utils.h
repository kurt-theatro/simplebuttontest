#ifndef UTILS_H
#define UTILS_H

using namespace std;


# define my_sizeof(type) ((char *)(&type+1)-(char*)(&type))


#define MAC_ADDR_SIZE 12

// If call to get voltage fails, default
#define DEFAULT_VOLTAGE 80 
#define DEFAULT_VOLUME 80
#define DEFAULT_CHARGING_STATE 100

enum testResult { pass,fail,pending,running};
#define PASS    "PASS"
#define FAIL    "FAIL"
#define RUNNING "RUNNING"
#define PENDING "PENDING"


#define MAX_LED_BRIGHTNESS 100

#define CAPTURE_VOLUME    80
#define PLAYBACK_VOLUME   90

extern const std::string GLOBAL_STRING;
extern std::string macAddress;

bool readLEDSetResult();
void upcaseStr(char *strParam);
void spawnRecording();
void spawnPlayback();
bool inRange(float lowRange, float highRange, float value);
std::string getMacAddress();
void playWavFile(std::string wavToPlay);
void powerDown();
void clearScreen();
char* stoupper( char* s );

const char* testResultToString(testResult result);
#endif


